import React, { useContext, useEffect } from 'react';
import {Route, Routes,} from 'react-router-dom';
// import ProtectedRoutes from "../utils/ProtectedRoutes.jsx";
// import UnAuthRoutes from "../utils/UnAuthRoutes.jsx";
// import { UserContext } from '../context/userContext.js';
import { ContextProvider } from '../contexts/ContextProvider.js';
import { AuthProvider } from '../authContext/index.js';
import LandingPage from '../LandingPage.js';
import LoginPage from '../pages/LoginPage.jsx';

export default function Router() {
    const {setUserLoggedin} = useContext(AuthProvider);
    // useEffect(()=>{
    // //  const user_id =  localStorage.getItem("user_id");
    // //  const username =  localStorage.getItem("username");
    //   if(userLoggedIn)  setUserLoggedin({user_id:user_id,username:username})
    // },[])
  return (
    <Routes>
    <Route path="/" element={<LoginPage />} />
    <Route path="/home" element={<LandingPage />} />
    </Routes>
  )
}
