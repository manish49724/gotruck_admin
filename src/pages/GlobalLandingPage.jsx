import React from 'react';
import { LandingPage } from './LandingPage'; // Assuming the LandingPage component is exported as named export

const GlobalLandingPage = () => {
  // You can add any additional logic or props here if needed
  return (
    <div>
      <LandingPage />
    </div>
  );
};

export default GlobalLandingPage;