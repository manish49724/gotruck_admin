import React, { useEffect, useState } from 'react';
import { BsCurrencyDollar } from 'react-icons/bs';
// import { GoPrimitiveDot } from 'react-icons/go';
import { IoIosMore } from 'react-icons/io';
import { DropDownListComponent } from '@syncfusion/ej2-react-dropdowns';

import { Stacked, Pie, Button, LineChart, SparkLine  , Navbar , Footer} from '../components';
import { earningData, medicalproBranding, recentTransactions, weeklyStats, dropdownData, SparklineAreaData, ecomPieChartData, Fetchdata, stackedChartData } from '../data/dummy';
import { useStateContext } from '../contexts/ContextProvider';
import product9 from '../data/product9.jpg';
import LandingPage from '../LandingPage';

const DropDown = ({ currentMode }) => (
  <div className="w-28 border-1 border-color px-2 py-1 rounded-md">
    <DropDownListComponent id="time" fields={{ text: 'Time', value: 'Id' }} style={{ border: 'none', color: (currentMode === 'Dark') && 'white' }} value="1" dataSource={dropdownData} popupHeight="220px" popupWidth="120px" />
  </div>
);

const Ecommerce = () => {
  const { currentColor, currentMode , activeMenu } = useStateContext();
  const [userInfo , setUserInfo] = useState();
  const [driverInfo , setDriverInfo] = useState();
  const [loading, setLoading] = useState(true);
  const [vehicleData , setVehicleData] = useState();
  const [shipmentData , setShipmentData] = useState(null);
  const [earningDatas , setEarningDatas] = useState([{}]);
  const [totalShipment , setTotalShipment] = useState('0');
  

  useEffect(() => {
    fetchDataAsync();
  }, []);
  
  useEffect(() => {
    biddingGraph();
  }, [shipmentData]);
  
  const fetchDataAsync = async () => {
      try {
        setLoading(true);
        console.log("Eccomerce Clicked --- ")
        const userInfo = await Fetchdata('Users');
        const driverInfo = await Fetchdata('Drivers');
        const vehicleInfo = await Fetchdata('Vehicles');
        const shipmentInfo = await Fetchdata('ShippingList');
        
        // Set individual state variables for each type of data
        setUserInfo(userInfo);
        setDriverInfo(driverInfo);
        setVehicleData(vehicleInfo);
        setShipmentData(shipmentInfo);
  
        // Set earningData after all data is fetched
        setEarningDatas(earningData({ userInfo, driverInfo, vehicleInfo, shipmentInfo }));
      } catch (error) {
        console.error('Error fetching data: ', error);
      }finally{
        setLoading(false)
      }
    };

  const biddingGraph = async () => {
    if(stackedChartData[0].length > 0){
      return null;
    }
    let totalPrice = 0;
    const monthlyPrices = {}; // Object to store accumulated prices for each month

 
    shipmentData?.forEach(entry => {
      const paymentDate = new Date(entry?.acceptedBids?.paymentDate);
      const month = paymentDate.toLocaleString('default', { month: 'short' });
      const price = entry.acceptedBids?.price;

      // Update the accumulated price for the month
      if (monthlyPrices[month]) {
        monthlyPrices[month] += price;
        totalPrice += parseFloat(price);
      } else {
        console.log("Price --- " , price)
        monthlyPrices[month] = price;
        if(price != undefined)
        totalPrice += parseFloat(price);
      }

    });

    // Convert accumulated prices into the desired format
    for (const month in monthlyPrices) {
      stackedChartData[0].push({ x: month, y: monthlyPrices[month] });
      SparklineAreaData.push({ x: month, y: monthlyPrices[month] });
    }

    setTotalShipment(totalPrice)
  
  }
  



  return (
    <div  className={currentMode !== 'Dark' ? 'dark' : ''}>
      <div className="flex relative dark:bg-main-dark-bg">
        <LandingPage/>
      <div
            className={
              activeMenu
                ? 'dark:bg-main-dark-bg  bg-main-bg min-h-screen md:ml-72 w-full  '
                : 'bg-main-bg dark:bg-main-dark-bg  w-full min-h-screen flex-2 '
            }
          >
            <div className="fixed md:static bg-main-bg dark:bg-main-dark-bg navbar w-full ">
              <Navbar />
            </div>
            <div>
        {/* Ecommerce Code  */}
        {loading ? (
              <div className="flex justify-center items-center min-h-screen">
                <div className="flex items-center justify-center">
                  <div className="w-16 h-16 border-4 border-t-4 border-gray-200 rounded-full animate-spin"></div>
                </div>
              </div>
            ) : 
    <div className="mt-24">
      <div className="flex flex-wrap lg:flex-nowrap justify-center ">
       
        <div className="flex m-3 flex-wrap justify-center gap-1 items-center">
          {earningDatas.map((item) => (
            <div key={item.title} className="bg-white h-44 dark:text-gray-200 dark:bg-secondary-dark-bg md:w-56  p-4 pt-9 rounded-2xl ">
              <button
                type="button"
                style={{ color: item.iconColor, backgroundColor: item.iconBg }}
                className="text-2xl opacity-0.9 rounded-full  p-4 hover:drop-shadow-xl"
              >
                {item.icon}
              </button>
              <p className="mt-3">
                <span className="text-lg font-semibold">{item.amount}</span>
                <span className={`text-sm text-${item.pcColor} ml-2`}>
                  {item.percentage}
                </span>
              </p>
              <p className="text-sm text-gray-400  mt-1">{item.title}</p>
            </div>
          ))}
        </div>
      </div>

      <div className="flex gap-10 flex-wrap justify-center">
        <div className="bg-white dark:text-gray-200 dark:bg-secondary-dark-bg m-3 p-4 rounded-2xl md:w-780  ">
          <div className="flex justify-between">
            <p className="font-semibold text-xl">Bidding Collection</p>
            <div className="flex items-center gap-4">
             
              <p className="flex items-center gap-2 text-green-400 hover:drop-shadow-xl">
                <span>
                  {/* <GoPrimitiveDot /> */}
                </span>
                <span>Graphical Data</span>
              </p>
            </div>
          </div>
          <div className="mt-10 flex gap-10 flex-wrap justify-center">
            <div className=" border-r-1 border-color m-4 pr-10">
              <div>
                <p>
                  <span className="text-3xl font-semibold">${totalShipment}</span>
                  <span className="p-1.5 hover:drop-shadow-xl cursor-pointer rounded-full text-white bg-green-400 ml-3 text-xs">
                    0%
                  </span>
                </p>
                <p className="text-gray-500 mt-1">In Dollars</p>
              </div>

              <div className="mt-5">
                <SparkLine currentColor={'#03C9D7'} id="line-sparkLine" type="Line" height="80px" width="250px" data={SparklineAreaData} color={currentColor} />
              </div>
            </div>
            <div>
              <Stacked currentMode={'blue-theme'} width="320px" height="360px" />
            </div>
          </div>
        </div>
        <div>
        </div>
      </div>


      <div className="flex flex-wrap justify-center">
       
        {/* <div className="w-400 bg-white dark:text-gray-200 dark:bg-secondary-dark-bg rounded-2xl p-6 m-3">
          <div className="flex justify-between">
            <p className="text-xl font-semibold">Daily Activities</p>
            <button type="button" className="text-xl font-semibold text-gray-500">
              <IoIosMore />
            </button>
          </div>
          <div className="mt-10">
            <img
              className="md:w-96 h-50 "
              src={product9}
              alt=""
            />
            <div className="mt-8">
              <p className="font-semibold text-lg">React 18 coming soon!</p>
              <p className="text-gray-400 ">By Johnathan Doe</p>
              <p className="mt-8 text-sm text-gray-400">
                This will be the small description for the news you have shown
                here. There could be some great info.
              </p>
              <div className="mt-3">
                <Button
                  color="white"
                  bgColor={currentColor}
                  text="Read More"
                  borderRadius="10px"
                />
              </div>
            </div>
          </div>
        </div> */}
      </div>
    
    
    </div>
}
    
    </div>
            <Footer />
          </div>
    </div>
    </div>
  );
};

export default Ecommerce;
