import React, { useEffect, useState } from 'react';
import { GridComponent, ColumnsDirective, ColumnDirective, Resize, Sort, ContextMenu, Filter, Page, ExcelExport, PdfExport, Edit, Inject } from '@syncfusion/ej2-react-grids';
import { Header, Navbar, Footer } from '../components';
import LandingPage from '../LandingPage';
import { useStateContext } from '../contexts/ContextProvider';
import { Fetchdata, contextMenuItems, ordersData, ordersGrid } from '../data/dummy';
import UserDetailsModal from './UserDetailModal';

const VehicleData = () => {
  const editing = { allowDeleting: true, allowEditing: true };
  const { currentMode, activeMenu } = useStateContext();
  const [userArray, setUserArray] = useState([]);
  const [userInfo , setUserInfo] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const fetchedUserInfo = await Fetchdata('Vehicles');
        setUserInfo(fetchedUserInfo);
      } catch (error) {
        console.error('Error fetching data: ', error);
      }
    };
    fetchData();
  }, []);

  const handleRowClick = (args) => {
    const selectedUserData = args.data;
    setSelectedUser(selectedUserData);
  };

  const handleCloseModal = () => {
    setSelectedUser(null);
  };

  return (
    <div className={currentMode !== 'Dark' ? 'dark' : ''}>
      {selectedUser && <UserDetailsModal user={selectedUser} onClose={handleCloseModal} />}
      <div className="flex relative dark:bg-main-dark-bg">
        <LandingPage />
        <div className={activeMenu ? 'dark:bg-main-dark-bg  bg-main-bg min-h-screen md:ml-72 w-full  ' : 'bg-main-bg dark:bg-main-dark-bg  w-full min-h-screen flex-2 '}>
          <div className="fixed md:static bg-main-bg dark:bg-main-dark-bg navbar w-full ">
            <Navbar />
          </div>
          <div>
            <div className="m-2 md:m-10 mt-24 p-2 md:p-10 bg-white rounded-3xl">
              <Header category="Page" title="Vehicle Details" />
              <GridComponent
                id="gridcomp"
                dataSource={userInfo}
                allowPaging
                allowSorting
                allowExcelExport
                allowPdfExport
                contextMenuItems={contextMenuItems}
                editSettings={editing}
                rowSelected={handleRowClick}
              >
                <ColumnsDirective>
                  <ColumnDirective headerText="Vehicle Model" field="vehicles.0.vehicleModel" textAlign="Center" width="120" />
                  <ColumnDirective headerText="Vehicle Type" field="vehicles.0.vehicleType" textAlign="Center" width="150" />
                  <ColumnDirective headerText="Registration Date" field="vehicles.0.registrationDate" textAlign="Center" width="150" />
                  <ColumnDirective headerText="Driver Name" field="vehicles.0.driverName" textAlign="Center" width="150" />
                  <ColumnDirective headerText="Driver ID" field="vehicles.0.driverId" textAlign="Center" width="120" />
                  <ColumnDirective headerText="Status" field="vehicles.0.status" textAlign="Center" width="150" />
                </ColumnsDirective>
                <Inject services={[Resize, Sort, ContextMenu, Filter, Page, ExcelExport, Edit, PdfExport]} />
              </GridComponent>
            </div>
            <Footer />
          </div>
        </div>
      </div>
    </div>
  );
};

export default VehicleData;
