import React, { useEffect, useState } from 'react';
import { GridComponent, ColumnsDirective, ColumnDirective, Resize, Sort, ContextMenu, Filter, Page, ExcelExport, PdfExport, Edit, Inject } from '@syncfusion/ej2-react-grids';
import { Header, Navbar, Footer } from '../components';
import LandingPage from '../LandingPage';
import { useStateContext } from '../contexts/ContextProvider';
import { Fetchdata, contextMenuItems, ordersData, ordersGrid } from '../data/dummy';
import UserDetailsModal from './UserDetailModal';

const Orders = () => {
  const editing = { allowDeleting: true, allowEditing: true };
  const { currentMode, activeMenu } = useStateContext();
  const [userArray, setUserArray] = useState([]);
  const [userInfo , setUserInfo] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const fetchedUserInfo = await Fetchdata('Users');
        // Filter out users with null, empty, or undefined role
        const filteredUserInfo = fetchedUserInfo.filter(user => user.role);
        setUserInfo(filteredUserInfo);
        const formattedUsers = filteredUserInfo.map(user => {
          // Filter the required fields
          return {
            name: `${user.firstName} ${user.lastName}`,
            email: user.email,
            country: user.country,
            phoneNumber: user.phoneNumber,
            license: user.licenseNumber,
            role: user.role,
            companyName: user.companyName
          };
        });
        setUserArray(formattedUsers);
      } catch (error) {
        console.error('Error fetching data: ', error);
      }
    };
    fetchData();
  }, []);

  const handleRowClick = (args) => {
    const selectedUserData = args.data;
    setSelectedUser(selectedUserData);
  };

  const handleCloseModal = () => {
    console.log("On closed Clicked --")
    setSelectedUser(false);
  };

  return (
    <div className={currentMode !== 'Dark' ? 'dark' : ''}>
    {selectedUser && <UserDetailsModal user={selectedUser} onClose={handleCloseModal} />}
      {console.log('userArray -------- ', userArray)}
      <div className="flex relative dark:bg-main-dark-bg">
        <LandingPage />
        <div className={activeMenu ? 'dark:bg-main-dark-bg  bg-main-bg min-h-screen md:ml-72 w-full  ' : 'bg-main-bg dark:bg-main-dark-bg  w-full min-h-screen flex-2 '}>
          <div className="fixed md:static bg-main-bg dark:bg-main-dark-bg navbar w-full ">
            <Navbar />
          </div>
          <div>
            <div className="m-2 md:m-10 mt-24 p-2 md:p-10 bg-white rounded-3xl">
              <Header category="Page" title="Users" />
              <GridComponent
                id="gridcomp"
                dataSource={userInfo}
                allowPaging
                allowSorting
                allowExcelExport
                allowPdfExport
                contextMenuItems={contextMenuItems}
                editSettings={editing}
                rowSelected={handleRowClick}
              >
                <ColumnsDirective>
                  <ColumnDirective headerText="Name" field="firstName" textAlign="Center" width="120" />
                  <ColumnDirective headerText="Email" field="email" textAlign="Center" width="150" />
                  <ColumnDirective headerText="Country" field="country" textAlign="Center" width="150" />
                  <ColumnDirective headerText="Phone Number" field="phoneNumber" textAlign="Center" width="150" />
                  <ColumnDirective headerText="License" field="licenseNumber" textAlign="Center" width="120" />
                  <ColumnDirective headerText="Role" field="role" textAlign="Center" width="120" />
                  <ColumnDirective headerText="Company Name" field="companyName" textAlign="Center" width="150" />
                </ColumnsDirective>
                <Inject services={[Resize, Sort, ContextMenu, Filter, Page, ExcelExport, Edit, PdfExport]} />
              </GridComponent>
            </div>
            <Footer />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Orders;
