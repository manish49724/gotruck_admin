import React from 'react';

const UserDetailsModal = ({ user, onClose }) => {
  const handleCloseModal = (e) => {
    if (e.target === e.currentTarget) {
      onClose();
    }
  };

  return (
    <div className="fixed top-0 left-0 flex items-center justify-center w-full h-full bg-gray-800 bg-opacity-50 z-50" onClick={handleCloseModal}>
      <div className="bg-white p-6 rounded-lg shadow-lg relative" onClick={(e) => e.stopPropagation()}>
        <span className="absolute top-0 right-0 p-2 text-lg cursor-pointer" onClick={onClose}>&times;</span>
        <h2 className="text-2xl font-semibold mb-4">User Details</h2>
        <ul>
          {Object.entries(user).map(([key, value]) => (
            <li key={key} className="mb-2">
              <strong className="mr-2">{key}: </strong>
              {Array.isArray(value) ? (
                <ul>
                  {value.map((item, index) => (
                    <li key={index}>
                      <ul>
                        {typeof item.shipperDetails === 'object' && item.shipperDetails !== null && Object.entries(item.shipperDetails).map(([subKey, subValue]) => (
                          <li key={subKey}>
                            <strong className="mr-2">{subKey}: </strong>
                            {subValue}
                          </li>
                        ))}
                      </ul>
                    </li>
                  ))}
                </ul>
              ) : (
                value
              )}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default UserDetailsModal;
