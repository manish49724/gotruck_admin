import React, { useState } from 'react';
import { getDocs, query, where, collection } from 'firebase/firestore';
import goTruckIcon from '../data/gotruck.ico';
import { useNavigate } from 'react-router-dom'; // Import useHistory hook
import { signInWithEmailAndPassword } from 'firebase/auth';
import { auth, db } from '../fetch/firebaseConfig';
import { Fetchdata } from '../data/dummy';
import { GiMailShirt } from 'react-icons/gi';
import { useStateContext } from '../contexts/ContextProvider';

const LoginPage = () => {
  
  const {setName , setEmailId} = useStateContext();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loading , setLoading] = useState(false);
  const navigate = useNavigate(); // Initialize useNavigate hook

  const handleLogin = async (e) => {
    
    e.preventDefault();
    try {
      setLoading(true);
      const userCredential = await signInWithEmailAndPassword(auth, username, password);
      const user = userCredential.user;
      console.log('User signed in:', user);
      console.log( '900000000 ------- '  , user.email)
      validateAdmin(user.email);
      setLoading(false);
      return user;
    } catch (error) {
      setLoading(false);
      console.error('Error signing in:', error);
        alert('Invalid username or password');
    }
  };

  const validateAdmin = async (email) => {
    
    try {
      // Query the 'Users' collection in Firestore to get user data
      const usersSnapshot = await getDocs(query(collection(db, 'Users')));
  
      // Convert the snapshot to an array of user objects
      const userInfo = usersSnapshot.docs.map(doc => doc.data());
      console.log('User Info:', userInfo);
  
      // Find the user object with the specified email
      const user = userInfo.find(user => user.email === email);
      console.log("Found User --- " , user);
      if (user) {
        const isAdmin = user.role === 'admin';
        console.log('user --------  role --- ' , user)
        if(isAdmin){
        setEmailId(user.email)
        setName(user.firstName)
        navigate('/dashboard' , { replace: true })
        }else{
          alert("Sorry! you don't have admin rights")
        }
      } else {
        alert("User not found!")
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };
  
  // Call the validateAdmin function with the email ID you want to check
  
  

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md w-full bg-white rounded-lg shadow-lg overflow-hidden">
        <div className="py-8 px-4 sm:px-6 lg:px-8">
          <img className="mx-auto h-12 w-auto" src={goTruckIcon} alt="Logo" />
          <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Sign in to gotruck</h2>
        </div>
        <div className="px-8 py-6">
          <form onSubmit={handleLogin}>
            <input type="hidden" name="remember" value="true" />
            <div className="space-y-6">
              <div>
                <label htmlFor="username" className="sr-only">Username</label>
                <input id="username" name="username" type="text" autoComplete="username" required
                       className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                       placeholder="Username" value={username} onChange={(e) => setUsername(e.target.value)} />
              </div>
              <div>
                <label htmlFor="password" className="sr-only">Password</label>
                <input id="password" name="password" type="password" autoComplete="current-password" required
                       className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                       placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} />
              </div>
            </div>

            <div className="flex items-center justify-between mt-4">
              <div className="flex items-center">
                <input id="remember-me" name="remember-me" type="checkbox"
                       className="h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded" />
                <label htmlFor="remember-me" className="ml-2 block text-sm text-gray-900">
                  Remember me
                </label>
              </div>

              {/* <div className="text-sm">
                <a href="/forgotPassword" className="font-medium text-indigo-600 hover:text-indigo-500">
                  Forgot your password?
                </a>
              </div> */}
            </div>

            <div className="mt-6">
              <button type="submit" className="w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
             
             { loading ?'Loading...' : 'Sign in' }
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
export default LoginPage;
