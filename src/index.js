import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './LandingPage';
import { BrowserRouter as Router, Routes, Route, BrowserRouter } from 'react-router-dom'; // Import necessary components from react-router-dom
import { ContextProvider } from './contexts/ContextProvider';
import LoginPage from './pages/LoginPage';
import ForgotPassword from './pages/ForgotPassword';
import { Area, Bar, ColorMapping, Ecommerce, Financial, Line, Pyramid } from './pages';
import { Pie, Stacked } from './components';
// import {Orders} from './components';
import {Orders} from './pages';
import Drivers from './pages/Driver';
import VehicleData from './pages/VehicleData';
import Shipment from './pages/Shipment';

ReactDOM.render(
  <React.StrictMode>
    <ContextProvider>
      <BrowserRouter>
      <div className='App'>
        <Routes>
          <Route path="/" element={<LoginPage />} />
          <Route path="/forgotPassword" element={<ForgotPassword />} />
                <Route path="/dashboard" element={<Ecommerce />} />

                {/* Pages */}
                <Route path="/users" element={<Orders />} /> 
                <Route path="/drivers" element={<Drivers />} /> 
                <Route path="/vehicledata" element={<VehicleData />} /> 
                <Route path="/shipments" element={<Shipment />} /> 
                {/* pages  */}
                {/* <Route path="/orders" element={<Orders />} />
                <Route path="/employees" element={<Employees />} />
                <Route path="/customers" element={<Customers />} /> */}

                {/* apps  */}
                {/* <Route path="/kanban" element={<Kanban />} />
                <Route path="/editor" element={<Editor />} />
                <Route path="/calendar" element={<Calendar />} />
                <Route path="/color-picker" element={<ColorPicker />} /> */}

                {/* charts  */}
                <Route path="/line" element={<Line />} />
                <Route path="/area" element={<Area />} />

                <Route path="/bar" element={<Bar />} />
                <Route path="/pie" element={<Pie />} />
                <Route path="/financial" element={<Financial />} />
                <Route path="/color-mapping" element={<ColorMapping />} />
                <Route path="/pyramid" element={<Pyramid />} />
                <Route path="/stacked" element={<Stacked />} />
          {/* Add more routes here if needed */}
        </Routes>
        </div>
      </BrowserRouter>
    </ContextProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);
