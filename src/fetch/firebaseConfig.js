// import { initializeApp } from 'firebase/firebase'; // Import initializeApp from firebase/app
// import { getAuth } from 'firebase/firebase-auth'; // Import getAuth from firebase/auth
import {initializeApp} from 'firebase/app';
import { getAuth , signInWithEmailAndPassword } from 'firebase/auth';
import { getFirestore, collection, getDocs } from 'firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyB9t4kg-Lxp870NZcM-DFjFiBI571RCyqE",
  authDomain: "gotruck-4b23c.firebaseapp.com",
  projectId: "gotruck-4b23c",
  storageBucket: "gotruck-4b23c.appspot.com",
  messagingSenderId: "477969737457",
  appId: "1:477969737457:web:3fabe9841338a9b23e6b50",
  measurementId: "G-Q90E1SS5RN"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Call initializeApp with your config object

const db = getFirestore(app);

// Get Firebase Auth
const auth = getAuth(app); // Call getAuth with your firebaseApp

// Export initialized firebaseApp and auth
export { db, auth , signInWithEmailAndPassword };
